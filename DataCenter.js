const APIs = {
    COVID19: "https://covid19-graphql.netlify.app/",
    SIGNALC: "https://signalc.herokuapp.com/graphql",
}

const myGraphQL = (APILINK, query, variables) => {
    return fetch(
        APILINK,
        {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ query, variables })
        }
    ).then(res => res.json());
}

export const sendCode = (phoneNumber) => {
    phoneNumber = "0558691496";
    return myGraphQL(
        APIs.SIGNALC,
        `mutation sendCode($input: loginUserInput) {
            loginUser(input: $input) {
                success
                message
            }
        }`,
        { input: { phone: phoneNumber } }
    ).then(response => {
        if (!response.data || !response.data.loginUser || response.data.loginUser.success !== true) {
            console.error(response);
            throw new Error("GRAPHQL_ERROR");
        }
    });
}

export const verifyCode = (phoneNumber, code) => {
    phoneNumber = "0558691496";
    return myGraphQL(
        APIs.SIGNALC,
        `mutation verifyCode($input: validateLoginUserInput) {
            validateLoginUser(input: $input) {
                mobileToken 
                user {
                    age
                    gender
                    lastCountriesVisited 
                    licenseNumber 
                }
            }
        }`,
        { input: { phone: phoneNumber, otp: code } }
    ).then(response => {
        if (response.data)
            return response.data.validateLoginUser;
        else
            return null;
    });
}

export const getNotifications = () => {
    return myGraphQL(
        APIs.SIGNALC,
        `{
            broadcastMessages {
              title
              description
              date: createdAt
            }
        }`,
    ).then(response => response.data.broadcastMessages);
}

export const getReports = () => {
    return myGraphQL(
        APIs.SIGNALC,
        `{
            reportedCases {
              target: reporting
              location
              date: createdAt
              landmark: nearestLandmark
              description
              user {
                phone
              }
            }
        }`,
    ).then(response => response.data.reportedCases.filter(c => c.user.phone === "0558691496"));
}

export const getVitals = () => {
    return myGraphQL(
        APIs.SIGNALC,
        `{
            vitals {
              user {
                phone
              }
              symptoms: vitals {
                dryCough
                tiredness
                soreThroat
                fever
                aches
                shortnessOfBreath
              }
              date: createdAt
            }
        }`,
    ).then(response => response.data.vitals.filter(v => v.user.phone === "0558691496"));
}

export const getCountries = () => {
    return myGraphQL(
        APIs.COVID19,
        `{
            countries {
                country
                countryInfo {
                    flag
                }
            }
        }`
    ).then(response => response.data.countries.map(coun => {
        return {
            name: coun.country,
            flag: coun.countryInfo.flag
        };
    }));
}

export const getWorldStats = () => {
    return myGraphQL(
        APIs.COVID19,
        `{
            globalTotal {
                confirmed: cases
                recovered
                deaths
            }
        }`
    ).then(response => response.data.globalTotal);
}

export const getCountryStats = (country) => {
    return myGraphQL(
        APIs.COVID19,
        `{
            country(name: "${country}") {
                result {
                    confirmed: cases
                    tests
                    deaths
                    active
                    recovered
                    critical
                    updated
                }
            }
        }`
    ).then(response => response.data.country.result);
}

export const getTestingCentres = () => {
    return myGraphQL(
        APIs.SIGNALC,
        `{
            testingSites {
              name
              address
              placesName
              location {coordinates}
            }
        }`
    ).then(response => response.data.testingSites);
}